import requests
def search_movie(*args):
    movie_name = " ".join(args)
    apikey = "57b67c8c"
    url = f"http://omdbapi.com?apikey={apikey}&t={movie_name}"

    result = requests.get(url)

    if result.status_code == 200:
        return result.json()
    else:
        print("404 not found")
        exit()