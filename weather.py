from weather import Weather, Unit
import requests
def weather_search(*args):
    weather = Weather(unit=Unit.CELSIUS)
    location = weather.lookup_by_location(sys.argv[2:])
    condition = location.condition
    return condition.text